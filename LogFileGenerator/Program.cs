﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;

namespace LogFileGenerator
{
    class Program
    {
        static Random random = new Random(1);
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+-=|<>          ";
        static string fp = string.Empty;

        static void Main(string[] args)
        {
            Random r = new Random(0);
            int i = 0;
            int count = 0;
            List<string> l = new List<string>()
            {
                "#INFO - X\r\n",
                "#DEBUG - X\r\n",
                "#WARN - X\r\n",
                "#ERROR - X\r\n",
                "#VERBOSE - X\r\n"
            };

            while (true)
            {
                try
                {
                    if (count-- == 0)
                    {
                        count = 40;
                        fp = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + $"\\LogFiles\\{DateTime.Now.ToLongTimeString().Replace(':', '-')}.txt";
                        Console.WriteLine($"\n *** Starting new log {fp} ***");
                    }

                    File.AppendAllText(fp, $"[{DateTime.Now.ToString("s")}]: " + l[i].Replace("X", RandomString()));
                    i = r.Next(l.Count);
                    Thread.Sleep(r.Next(100, 500));
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception: " + ex.Message);
                }
            }
        }

        static string RandomString()
        {
            return new string(Enumerable.Repeat(chars, random.Next(60))
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
