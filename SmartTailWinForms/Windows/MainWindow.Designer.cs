﻿namespace SmartTail
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnOpen = new System.Windows.Forms.Button();
            this.comboBoxTheme = new System.Windows.Forms.ComboBox();
            this.lblTheme = new System.Windows.Forms.Label();
            this.textBoxAutoLoad = new System.Windows.Forms.TextBox();
            this.buttonAutoLoad = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.lblHidden = new System.Windows.Forms.Label();
            this.textBoxProcessName = new System.Windows.Forms.TextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btnHighlight = new System.Windows.Forms.Button();
            this.btnFilter = new System.Windows.Forms.Button();
            this.cbxAutoDisplay = new System.Windows.Forms.CheckBox();
            this.btnCapture = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Text files|*.txt";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // btnOpen
            // 
            this.btnOpen.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnOpen.ForeColor = System.Drawing.Color.Black;
            this.btnOpen.Location = new System.Drawing.Point(240, 5);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(70, 24);
            this.btnOpen.TabIndex = 9;
            this.btnOpen.Text = "Open File";
            this.btnOpen.UseVisualStyleBackColor = false;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // comboBoxTheme
            // 
            this.comboBoxTheme.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxTheme.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxTheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTheme.ForeColor = System.Drawing.Color.Black;
            this.comboBoxTheme.FormattingEnabled = true;
            this.comboBoxTheme.Items.AddRange(new object[] {
            "Light",
            "Dark",
            "Golf",
            "Winter",
            "Mint",
            "Sci-Fi"});
            this.comboBoxTheme.Location = new System.Drawing.Point(828, 7);
            this.comboBoxTheme.Name = "comboBoxTheme";
            this.comboBoxTheme.Size = new System.Drawing.Size(63, 21);
            this.comboBoxTheme.TabIndex = 4;
            this.comboBoxTheme.SelectedIndexChanged += new System.EventHandler(this.comboBoxTheme_SelectedIndexChanged);
            // 
            // lblTheme
            // 
            this.lblTheme.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTheme.AutoSize = true;
            this.lblTheme.BackColor = System.Drawing.Color.Transparent;
            this.lblTheme.ForeColor = System.Drawing.Color.White;
            this.lblTheme.Location = new System.Drawing.Point(782, 10);
            this.lblTheme.Name = "lblTheme";
            this.lblTheme.Size = new System.Drawing.Size(40, 13);
            this.lblTheme.TabIndex = 3;
            this.lblTheme.Text = "Theme";
            // 
            // textBoxAutoLoad
            // 
            this.textBoxAutoLoad.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textBoxAutoLoad.BackColor = System.Drawing.Color.White;
            this.textBoxAutoLoad.ForeColor = System.Drawing.Color.Black;
            this.textBoxAutoLoad.Location = new System.Drawing.Point(316, 7);
            this.textBoxAutoLoad.Name = "textBoxAutoLoad";
            this.textBoxAutoLoad.ReadOnly = true;
            this.textBoxAutoLoad.Size = new System.Drawing.Size(248, 20);
            this.textBoxAutoLoad.TabIndex = 6;
            // 
            // buttonAutoLoad
            // 
            this.buttonAutoLoad.BackColor = System.Drawing.SystemColors.ControlLight;
            this.buttonAutoLoad.ForeColor = System.Drawing.Color.Black;
            this.buttonAutoLoad.Location = new System.Drawing.Point(164, 5);
            this.buttonAutoLoad.Name = "buttonAutoLoad";
            this.buttonAutoLoad.Size = new System.Drawing.Size(70, 24);
            this.buttonAutoLoad.TabIndex = 7;
            this.buttonAutoLoad.Text = "Auto-Load";
            this.buttonAutoLoad.UseVisualStyleBackColor = false;
            this.buttonAutoLoad.Click += new System.EventHandler(this.buttonAutoLoad_Click);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.ShowNewFolderButton = false;
            // 
            // lblHidden
            // 
            this.lblHidden.Location = new System.Drawing.Point(0, 0);
            this.lblHidden.Name = "lblHidden";
            this.lblHidden.Size = new System.Drawing.Size(100, 23);
            this.lblHidden.TabIndex = 18;
            // 
            // textBoxProcessName
            // 
            this.textBoxProcessName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxProcessName.BackColor = System.Drawing.Color.White;
            this.textBoxProcessName.ForeColor = System.Drawing.Color.Black;
            this.textBoxProcessName.Location = new System.Drawing.Point(670, 7);
            this.textBoxProcessName.Name = "textBoxProcessName";
            this.textBoxProcessName.Size = new System.Drawing.Size(94, 20);
            this.textBoxProcessName.TabIndex = 13;
            this.textBoxProcessName.Validated += new System.EventHandler(this.textBoxProcessName_Validated);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.BackColor = System.Drawing.Color.White;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.ForeColor = System.Drawing.Color.Black;
            this.richTextBox1.HideSelection = false;
            this.richTextBox1.Location = new System.Drawing.Point(0, 33);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(988, 478);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            this.richTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.richTextBox1_KeyDown);
            this.richTextBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.richTextBox1_MouseUp);
            // 
            // btnHighlight
            // 
            this.btnHighlight.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnHighlight.ForeColor = System.Drawing.Color.Black;
            this.btnHighlight.Location = new System.Drawing.Point(12, 5);
            this.btnHighlight.Name = "btnHighlight";
            this.btnHighlight.Size = new System.Drawing.Size(70, 24);
            this.btnHighlight.TabIndex = 14;
            this.btnHighlight.Text = "Highlight";
            this.btnHighlight.UseVisualStyleBackColor = false;
            this.btnHighlight.Click += new System.EventHandler(this.btnHighlight_Click);
            // 
            // btnFilter
            // 
            this.btnFilter.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnFilter.ForeColor = System.Drawing.Color.Black;
            this.btnFilter.Location = new System.Drawing.Point(88, 5);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(70, 24);
            this.btnFilter.TabIndex = 15;
            this.btnFilter.Text = "Filter";
            this.btnFilter.UseVisualStyleBackColor = false;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // cbxAutoDisplay
            // 
            this.cbxAutoDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxAutoDisplay.AutoSize = true;
            this.cbxAutoDisplay.ForeColor = System.Drawing.Color.White;
            this.cbxAutoDisplay.Location = new System.Drawing.Point(579, 10);
            this.cbxAutoDisplay.Name = "cbxAutoDisplay";
            this.cbxAutoDisplay.Size = new System.Drawing.Size(85, 17);
            this.cbxAutoDisplay.TabIndex = 16;
            this.cbxAutoDisplay.Text = "Auto-Display";
            this.cbxAutoDisplay.UseVisualStyleBackColor = true;
            this.cbxAutoDisplay.CheckedChanged += new System.EventHandler(this.cbxAutoDisplay_CheckedChanged);
            // 
            // btnCapture
            // 
            this.btnCapture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCapture.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnCapture.ForeColor = System.Drawing.Color.Black;
            this.btnCapture.Location = new System.Drawing.Point(906, 5);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(70, 24);
            this.btnCapture.TabIndex = 17;
            this.btnCapture.Text = "Capture";
            this.btnCapture.UseVisualStyleBackColor = false;
            this.btnCapture.Click += new System.EventHandler(this.btnCapture_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(988, 509);
            this.Controls.Add(this.btnCapture);
            this.Controls.Add(this.cbxAutoDisplay);
            this.Controls.Add(this.btnFilter);
            this.Controls.Add(this.btnHighlight);
            this.Controls.Add(this.textBoxProcessName);
            this.Controls.Add(this.lblHidden);
            this.Controls.Add(this.buttonAutoLoad);
            this.Controls.Add(this.textBoxAutoLoad);
            this.Controls.Add(this.lblTheme);
            this.Controls.Add(this.comboBoxTheme);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.richTextBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 200);
            this.Name = "Form1";
            this.Text = "Smart Tail";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Move += new System.EventHandler(this.Form1_Move);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.ComboBox comboBoxTheme;
        private System.Windows.Forms.Label lblTheme;
        private System.Windows.Forms.TextBox textBoxAutoLoad;
        private System.Windows.Forms.Button buttonAutoLoad;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label lblHidden;
        private System.Windows.Forms.TextBox textBoxProcessName;
        private System.Windows.Forms.Button btnHighlight;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.CheckBox cbxAutoDisplay;
        private System.Windows.Forms.Button btnCapture;
    }
}

