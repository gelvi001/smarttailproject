﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Threading.Tasks;

namespace SmartTail
{
    public partial class MainWindow : Form
    {
        #region Declarations

        private Size normalSize;
        private Point normalLocation;

        private FormWindowState restoreState;
        bool appWasRunning;
        private bool autoload;
        private bool autodisplay;

        public static Color fBackColor;
        public static Color fForeColor;
        public static Color fTrim;

        private string processName = null;

        private FileScanner _FileScaner = new FileScanner();
        private object _lock = new object();
        public static List<HighlightRule> _highlightingRules = new List<HighlightRule>();
        public static List<FilterRule> _filteringRules = new List<FilterRule>();

        #endregion

        #region Form methods

        public MainWindow()
        {
            InitializeComponent();

            normalSize = this.Size;
            normalLocation = this.Location;
            restoreState = this.WindowState;

            _FileScaner.ContentUpdated += _FileScaner_ContentUpdated;
            _FileScaner.NewFileFound += _FileScaner_NewFileFound;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // read from config file, if it exists
            try
            {
                comboBoxTheme.SelectedIndex = 0;
                if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\SmartTail\\st_config.txt"))
                    return;

                FileStream fs = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\SmartTail\\st_config.txt", FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(fs);

                string[] configsAndRules = sr.ReadToEnd().Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                string[] configs = configsAndRules[0]?.Split(new char[] { ';' });
                var highlightRules = configsAndRules.Length > 1 ? configsAndRules[1] : string.Empty;
                var filterRules = configsAndRules.Length > 2 ? configsAndRules[2] : string.Empty;
                sr.Close();
                fs.Close();

                if (configs == null || configs.Length != 9)
                {
                    MessageBox.Show("Number of params: " + configs.Length + "\nNumber required: 9", "Error");
                    return;
                }

                // theme
                if (!string.IsNullOrEmpty(configs[0]) && Convert.ToInt16(configs[0]) >= 0)
                    comboBoxTheme.SelectedIndex = Convert.ToInt16(configs[0]);

                // directory path
                if (!string.IsNullOrEmpty(configs[1]))
                {
                    autoload = true;
                    textBoxAutoLoad.Text = configs[1];
                    textBoxAutoLoad.SelectionStart = textBoxAutoLoad.TextLength;
                    textBoxAutoLoad.ScrollToCaret();
                    startTailAutoLoad();
                }

                // read highlight rules
                if (highlightRules != null)
                {
                    foreach (var rule in highlightRules.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries)) // process each rule
                    {
                        string[] parameters = rule.Split('\t');

                        //enabled
                        bool enabled = (parameters[0].ToLower() == "true") ? true : false;
                        //expression
                        string expression = parameters[1];
                        //forecolor
                        string[] rgb = parameters[2].Split(',');
                        Color fc = Color.FromArgb(Convert.ToInt16(rgb[0]), Convert.ToInt16(rgb[1]), Convert.ToInt16(rgb[2]));
                        //backcolor
                        rgb = parameters[3].Split(',');
                        Color bc = Color.FromArgb(Convert.ToInt16(rgb[0]), Convert.ToInt16(rgb[1]), Convert.ToInt16(rgb[2]));
                        //forecolor
                        _highlightingRules.Add(new HighlightRule(enabled, expression, new ColorPair(fc, bc)));
                    }
                }

                // read filter rules
                if (filterRules != null)
                {
                    foreach (var rule in filterRules.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries)) // process each rule
                    {
                        string[] parameters = rule.Split('\t');

                        //enabled
                        bool enabled = (parameters[0].ToLower() == "true") ? true : false;
                        //expression
                        string expression = parameters[1];
                        _filteringRules.Add(new FilterRule(enabled, expression));
                    }
                }

                // process name
                if (!string.IsNullOrEmpty(configs[8]))
                {
                    processName = configs[8];
                    textBoxProcessName.Text = processName;
                }
                // auto display
                if (!string.IsNullOrEmpty(configs[6]))
                    autodisplay = cbxAutoDisplay.Checked = configs[6].Equals("1");

                // size, position, state
                if (!string.IsNullOrEmpty(configs[4]) && !string.IsNullOrEmpty(configs[5]))
                {
                    string[] sizes = configs[4].Split(',');
                    Size size = new Size(Convert.ToInt16(sizes[0]), Convert.ToInt16(sizes[1]));

                    string[] locations = configs[5].Split(',');
                    Point location = new Point(Convert.ToInt16(locations[0]), Convert.ToInt16(locations[1]));

                    //check if previous form location is within available screen(s)
                    Rectangle formRect = new Rectangle(location, size);
                    bool isVisible = false;
                    foreach (Screen screen in Screen.AllScreens) // check each screen to see if part of the form is visible
                    {
                        if (formRect.Left + 50 < screen.Bounds.Right
                        && formRect.Right - 50 > screen.Bounds.Left
                        && formRect.Top + 50 < screen.Bounds.Bottom
                        && formRect.Bottom - 50 > screen.Bounds.Top)
                        {
                            isVisible = true;
                        }
                    }
                    if (!isVisible)  // if not visible, push into screen
                    {
                        Screen s = Screen.FromRectangle(formRect);
                        if (formRect.Left + 50 >= s.Bounds.Right)
                            location.X = s.Bounds.Right - formRect.Width;
                        if (formRect.Right - 50 <= s.Bounds.Left)
                            location.X = s.Bounds.Left;
                        if (formRect.Top + 50 >= s.Bounds.Bottom)
                            location.Y = s.Bounds.Bottom - formRect.Height;
                        if (formRect.Bottom - 50 <= s.Bounds.Top)
                            location.Y = s.Bounds.Top;
                    }
                    this.Size = size;
                    this.Location = location;
                }

                if (!string.IsNullOrEmpty(configs[3]))
                    this.WindowState = (configs[3].Equals("0")) ? FormWindowState.Normal : FormWindowState.Maximized;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            // update config file

            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\SmartTail"))
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\SmartTail");

            FileStream fs = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\SmartTail\\st_config.txt", FileMode.Create, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);

            // Opacity, theme, Directory Path, Auto Load, Auto Scroll, state, size, position
            
            // get state and size
            string state = (this.WindowState == FormWindowState.Maximized) ? "1" : "0";
            string size = normalSize.Width.ToString() + "," + normalSize.Height.ToString();
            string position = normalLocation.X.ToString() + "," + normalLocation.Y.ToString();
            string autoDisplayEnabled = autodisplay ? "1" : "0";
            //string highlight = (lblHightlight.Text == HL) ? "1" : "0";
            
            // write to file
            sw.Write(comboBoxTheme.SelectedIndex.ToString() + ";"
                + textBoxAutoLoad.Text + ";"
                + autoload.ToString() + ";"
                + state + ";"
                + size + ";"
                + position + ";"
                + autoDisplayEnabled + ";"
                + "0" + ";" // highlight
                + processName);

            // add highlighting rules
            sw.Write("|");
            foreach (HighlightRule rule in _highlightingRules)
            {
                sw.Write("\n" + rule.Enabled.ToString() + "\t"
                    + rule.Expression + "\t"
                    + rule.Colors.ForeColor.R + ","
                        + rule.Colors.ForeColor.G + ","
                        + rule.Colors.ForeColor.B + "\t"
                    + rule.Colors.BackColor.R + ","
                        + rule.Colors.BackColor.G + ","
                        + rule.Colors.BackColor.B);
            }

            // add filter rules
            sw.Write("|");
            foreach (FilterRule rule in _filteringRules)
            {
                sw.Write("\n" + rule.Enabled.ToString() + "\t"
                    + rule.Expression);
            }

            sw.Close();
            fs.Close();

            _FileScaner.StopScanning();
            while (_FileScaner.IsScanning)
                Thread.Sleep(5);
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                normalSize = this.Size;
                restoreState = FormWindowState.Normal;
            }
            else if (this.WindowState == FormWindowState.Maximized)
                restoreState = FormWindowState.Maximized;
        }

        private void Form1_Move(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                normalLocation = this.Location;
            }
        }

        #endregion

        #region Tail methods

        private void _FileScaner_NewFileFound(object sender, string e)
        {
            try
            {
                lock (_lock)
                {
                    this.Text = e;
                    richTextBox1.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Exception _FileScaner_NewFileFound: {ex.Message}\r\n");
            }
        }

        private void _FileScaner_ContentUpdated(object sender, string text)
        {
            try
            {
                lock (_lock)
                {
                    int start = (richTextBox1.SelectionStart == richTextBox1.Text.Length) ? -1 : richTextBox1.SelectionStart;
                    int len = richTextBox1.SelectionLength;
                    int endIndex = richTextBox1.Text.Length;

                    if (_filteringRules.Count(x => x.Enabled) > 0)
                    {
                        text = applyFilterRules(text);
                    }

                    richTextBox1.AppendText((string)text);
                    if (!this.Focused)
                    {
                        richTextBox1.SelectionStart = richTextBox1.Text.Length;
                        richTextBox1.ScrollToCaret();
                    }
                    if (_highlightingRules.Count(x => x.Enabled) > 0)
                        highlight(endIndex);

                    if (start != -1)
                        richTextBox1.Select(start, len);

                    if (MouseButtons == MouseButtons.None && !isAtBottom())
                    {
                        richTextBox1.SelectionStart = richTextBox1.Text.Length;
                        richTextBox1.ScrollToCaret();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Exception _FileScaner_ContentUpdated: {ex.Message}\r\n");
            }

        }

        private void startTail(string filePath)
        {
            try
            {
                richTextBox1.Clear();

                this.Text = filePath;
                _FileScaner.FilePath = filePath;
                _FileScaner.AutoLoad = false;
                if (!_FileScaner.IsScanning)
                    _FileScaner.StartScan();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void startTailAutoLoad()
        {
            try
            {
                // check if directory exists
                if (!Directory.Exists(textBoxAutoLoad.Text))
                {
                    MessageBox.Show("Directory no longer exists:\n" + textBoxAutoLoad.Text, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    autoload = false;
                    textBoxAutoLoad.Text = string.Empty;
                    return;
                }

                richTextBox1.Clear();
                this.Text = string.Empty;
                _FileScaner.FolderPath = textBoxAutoLoad.Text;
                _FileScaner.AutoLoad = true;
                if (!_FileScaner.IsScanning)
                    _FileScaner.StartScan();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool isAtBottom()
        {
            int line_total = richTextBox1.GetLineFromCharIndex(richTextBox1.GetCharIndexFromPosition(new Point(richTextBox1.Bounds.Right, richTextBox1.Bounds.Bottom)));
            int line_lastVisible = richTextBox1.GetLineFromCharIndex(richTextBox1.TextLength - 1);

            return line_total == line_lastVisible;
        }

        #endregion

        #region Open methods

        private void btnOpen_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            autoload = false;
            startTail(openFileDialog1.FileName);
        }

        #endregion

        #region Theme methods

        private void comboBoxTheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBoxTheme.SelectedIndex)
            {
                case 0:
                    updateTheme(new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))),
                        Color.White, Color.Black, Color.SteelBlue, 100);
                    break;
                case 1:
                    updateTheme(new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))),
                        Color.Black, Color.Crimson, Color.Black, 100);
                    break;
                case 2:
                    updateTheme(new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))),
                        Color.Green, Color.White, Color.Black, 100);
                    break;
                case 3:
                    updateTheme(new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0))),
                        Color.LightCyan, Color.DarkBlue, Color.Black, 100);
                    break;
                case 4:
                    updateTheme(new System.Drawing.Font("Courier New", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))),
                        Color.LightGreen, Color.DarkGreen, Color.DarkGreen, 100);
                    break;
                case 5:
                    updateTheme(new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))),
                        Color.Black, System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0))))), Color.DimGray, 85);
                    break;
                default:
                    MessageBox.Show("Invalid index!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        private void updateTheme(Font font, Color backColor, Color foreColor, Color trim, int opacity)
        {
            try
            {
                this.richTextBox1.Font = font;
                this.richTextBox1.BackColor = backColor;
                this.richTextBox1.ForeColor = foreColor;

                this.btnOpen.BackColor = backColor;
                this.btnOpen.ForeColor = (backColor == Color.White
                    || backColor == Color.LightCyan
                    || backColor == Color.LightGreen) ? Color.Black : Color.White;

                this.textBoxAutoLoad.BackColor = backColor;
                this.textBoxAutoLoad.ForeColor = (backColor == Color.White
                    || backColor == Color.LightCyan
                    || backColor == Color.LightGreen) ? Color.Black : Color.White;

                this.textBoxProcessName.BackColor = backColor;
                this.textBoxProcessName.ForeColor = (backColor == Color.White
                    || backColor == Color.LightCyan
                    || backColor == Color.LightGreen) ? Color.Black : Color.White;

                this.buttonAutoLoad.BackColor = backColor;
                this.buttonAutoLoad.ForeColor = (backColor == Color.White
                    || backColor == Color.LightCyan
                    || backColor == Color.LightGreen) ? Color.Black : Color.White;

                this.btnHighlight.BackColor = backColor;
                this.btnHighlight.ForeColor = (backColor == Color.White
                                             || backColor == Color.LightCyan
                                             || backColor == Color.LightGreen) ? Color.Black : Color.White;

                this.btnFilter.BackColor = backColor;
                this.btnFilter.ForeColor = (backColor == Color.White
                                          || backColor == Color.LightCyan
                                          || backColor == Color.LightGreen) ? Color.Black : Color.White;

                this.btnCapture.BackColor = backColor;
                this.btnCapture.ForeColor = (backColor == Color.White
                                            || backColor == Color.LightCyan
                                            || backColor == Color.LightGreen) ? Color.Black : Color.White;

                this.BackColor = trim;
                this.Opacity = Convert.ToDouble(opacity) / 100;

                // set static variables
                fBackColor = backColor;
                fForeColor = foreColor;
                fTrim = trim;

                if (_highlightingRules.Count(x => x.Enabled) > 0)
                    highlight(0);
                else
                    removeHighlight();

                int start = richTextBox1.SelectionStart;
                int len = richTextBox1.SelectionLength;

                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                richTextBox1.ScrollToCaret();

                if (start != richTextBox1.Text.Length)
                    richTextBox1.Select(start, len);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Auto Load

        private void buttonAutoLoad_Click(object sender, EventArgs e)
        {
            showFolderBrowserDialog();
        }

        private void showFolderBrowserDialog()
        {
            folderBrowserDialog1.ShowDialog();

            if (string.IsNullOrEmpty(folderBrowserDialog1.SelectedPath))
            {
                autoload = false;
                return;
            }

            autoload = true;
            textBoxAutoLoad.Text = folderBrowserDialog1.SelectedPath;
            textBoxAutoLoad.SelectionStart = textBoxAutoLoad.TextLength;
            textBoxAutoLoad.ScrollToCaret();

            startTailAutoLoad();
        }

        #endregion
        
        #region highlight methods

        private void btnHighlight_Click(object sender, EventArgs e)
        {
            HighlightRules settings = new HighlightRules();
            settings.ShowDialog();

            if (_highlightingRules.Count > 0)
                highlight(0);
            else
                removeHighlight();
        }

        private void highlight(int startPosition)
        {
            try
            {
                // get all matches
                var matches = GetHighlightMatches(richTextBox1.Text, startPosition);

                int StartCursorPosition = richTextBox1.SelectionStart;
                int StartCursorLength = richTextBox1.SelectionLength;

                // reset colors
                richTextBox1.Select(startPosition, richTextBox1.Text.Length - 1);
                richTextBox1.SelectionColor = richTextBox1.ForeColor;
                richTextBox1.SelectionBackColor = richTextBox1.BackColor;

                foreach (HighlightMatch match in matches)
                {
                    richTextBox1.Select(match.Start, match.Length);
                    richTextBox1.SelectionColor = match.Colors.ForeColor;
                    richTextBox1.SelectionBackColor = match.Colors.BackColor;
                }

                richTextBox1.SelectionStart = StartCursorPosition;
                richTextBox1.SelectionLength = StartCursorLength;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private List<HighlightMatch> GetHighlightMatches(string text, int start)
        {
            List<HighlightMatch> matches = new List<HighlightMatch>();
            foreach (HighlightRule rule in _highlightingRules)
            {
                if (rule.Enabled)
                {
                    Regex regex = new Regex(rule.Expression);
                    MatchCollection collection = (start == 0) ? regex.Matches(text)
                        : regex.Matches(text.Substring(start));
                    foreach (Match m in collection)
                        matches.Add(new HighlightMatch(m.Index + start, m.Length, rule.Colors));
                }
            }

            if (matches.Count != 0)
                matches = matches.OrderBy(x => x.Start).ToList();

            return matches;
        }

        private void removeHighlight()
        {
            int StartCursorPosition = richTextBox1.SelectionStart;
            int StartCursorLength = richTextBox1.SelectionLength;

            richTextBox1.Select(0, richTextBox1.Text.Length - 1);
            richTextBox1.SelectionColor = richTextBox1.ForeColor;
            richTextBox1.SelectionBackColor = richTextBox1.BackColor;

            richTextBox1.SelectionStart = StartCursorPosition;
            richTextBox1.SelectionLength = StartCursorLength;
        }

        #endregion

        #region auto display

        private void cbxAutoDisplay_CheckedChanged(object sender, EventArgs e)
        {
            autodisplay = cbxAutoDisplay.Checked;

            if (autodisplay)
                startAutoDisplay();
        }


        private void textBoxProcessName_Validated(object sender, EventArgs e)
        {
            processName = textBoxProcessName.Text;
        }

        private void checkAutoDisplay()
        {
            // Check if AD app is running and restore, if not, minimize
            if (autodisplay && !string.IsNullOrEmpty(processName))
            {
                System.Diagnostics.Process[] p = System.Diagnostics.Process.GetProcessesByName(processName);

                bool appIsRunning = (p.Length == 0) ? false : true;

                if (!appIsRunning)
                {
                    if (appWasRunning && this.WindowState != FormWindowState.Minimized)
                        this.WindowState = FormWindowState.Minimized;
                    appWasRunning = false;
                }
                else if (appIsRunning)
                {
                    if (!appWasRunning && this.WindowState == FormWindowState.Minimized)
                        this.WindowState = restoreState;
                    appWasRunning = true;
                }
            }
            else
                appWasRunning = false;
        }

        private async void startAutoDisplay()
        {
            while (autodisplay)
            {
                checkAutoDisplay();
                await Task.Run(() => Thread.Sleep(250));
            }
        }

        #endregion

        #region snippet

        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S && e.Modifiers == Keys.Control && richTextBox1.SelectedText.Length > 0)
                showSmallText(richTextBox1.SelectedText);
        }

        private void showSmallText(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                Snippet st = new Snippet(text);
                st.Show(this);
            }
        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            if (richTextBox1.SelectedText.Length > 0)
                showSmallText(richTextBox1.SelectedText);
        }

        private void richTextBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right && richTextBox1.SelectedText.Length > 0)
                showSmallText(richTextBox1.SelectedText);
        }

        #endregion


        private void btnFilter_Click(object sender, EventArgs e)
        {
            FilterRules settings = new FilterRules();
            settings.ShowDialog();
        }

        private string applyFilterRules(string input)
        {
            var expression = string.Join("|", _filteringRules.Where(x => x.Enabled).Select(x => x.Expression));
            Regex regex = new Regex(expression);
            MatchCollection collection = regex.Matches(input);

            // determine ranges (in case over-lapping results)
            var ranges = new List<IEnumerable<int>>();
            foreach (Match m in collection)
                ranges.Add(Enumerable.Range(m.Index, m.Length));

            var all = new List<int>();
            foreach (var range in ranges)
                all = all.Union(range).ToList();

            var output = string.Empty;
            if (all.Count > 0)
            {
                var startEnds = new List<(int start, int end)>();
                int start = all.Min();
                int index = start;
                foreach (var next in all.OrderBy(x => x))
                {
                    if (next > index + 1 || next == all.Max())
                    {
                        startEnds.Add((start, next == all.Max() ? next : index));
                        start = next;
                    }

                    index = next;
                }

                foreach (var range in startEnds)
                    output += input.Substring(range.start, range.end - range.start + 1);
            }

            return output;
        }
    }
}
