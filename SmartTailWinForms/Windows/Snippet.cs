﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace SmartTail
{
    public partial class Snippet : Form
    {
        public Snippet(string text)
        {
            InitializeComponent();

            richTextBox1.AppendText(text);

            this.richTextBox1.KeyDown += new KeyEventHandler(richTextBox1_KeyDown);
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S && e.Modifiers == Keys.Control)
            {
                saveFile();
            }
        }

        private void saveFile()
        {
            try
            {
                saveFileDialog1.FileName = "*.txt";

                if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create, FileAccess.Write);
                    StreamWriter sw = new StreamWriter(fs);

                    sw.Write(richTextBox1.Text.Replace("\n", "\r\n"));

                    sw.Close();
                    fs.Close();

                    this.Text = saveFileDialog1.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
    }
}
