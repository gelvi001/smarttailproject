﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace SmartTail
{
    public partial class FilterRules : Form
    {
        public FilterRules()
        {
            InitializeComponent();

            this.dataGridView1.CellClick += new DataGridViewCellEventHandler(dataGridView1_CellClick);
            this.dataGridView1.CellValidated += new DataGridViewCellEventHandler(dataGridView1_CellValidated);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int col = e.ColumnIndex;
            int row = e.RowIndex;

            if (col == 2 && dataGridView1.Rows.GetLastRow(DataGridViewElementStates.Visible) != row && row != -1) // delete
            {
                dataGridView1.Rows.Remove(dataGridView1.Rows[row]);
            }
        }

        private void dataGridView1_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCell currentCell = dataGridView1.Rows[e.RowIndex].Cells[1];
            if (e.ColumnIndex == 1 && currentCell.Value != null)
            {
                // test regex for erors
                if (!isRegexValid((string)currentCell.Value))
                {
                    currentCell.ErrorText = "Invalid RegEx";
                }
                else
                {
                    currentCell.ErrorText = String.Empty;

                    // check enabled
                    dataGridView1.Rows[e.RowIndex].Cells[0].Value = true;
                }
            }
            else if (e.ColumnIndex == 1 && !String.IsNullOrEmpty(currentCell.ErrorText))
                currentCell.ErrorText = String.Empty;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            // save to file and update highlighting

            List<FilterRule> rules = new List<FilterRule>();
            string errors = string.Empty;

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (row.Cells[1].Value != null 
                    && row.Cells[1].ErrorText == string.Empty)
                {
                    rules.Add(new FilterRule(Convert.ToBoolean(row.Cells[0].Value), (string)row.Cells[1].Value));
                }
                else if (row.Cells[1].Value != null && row.Cells[1].ErrorText != string.Empty)
                {
                    if (errors != string.Empty)
                        errors += "\n";
                    errors += "Invalid RegEx: " + (string)row.Cells[1].Value;
                }
            }

            if (errors != string.Empty)
            {
                MessageBox.Show(errors, "Errors Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MainWindow._filteringRules = rules;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            // set theme
            this.BackColor = MainWindow.fTrim;
            this.dataGridView1.BackgroundColor = MainWindow.fBackColor;

            this.buttonCancel.BackColor = MainWindow.fBackColor;
            this.buttonCancel.ForeColor = (MainWindow.fBackColor == Color.White 
                                            || MainWindow.fBackColor == Color.LightCyan
                                            || MainWindow.fBackColor == Color.LightGreen) ? Color.Black : Color.White;

            this.buttonSave.BackColor = MainWindow.fBackColor;
            this.buttonSave.ForeColor = (MainWindow.fBackColor == Color.White 
                                            || MainWindow.fBackColor == Color.LightCyan
                                            || MainWindow.fBackColor == Color.LightGreen) ? Color.Black : Color.White;

            // read in rules
            foreach (FilterRule rule in MainWindow._filteringRules)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGridView1);
                row.Cells[0].Value = rule.Enabled;
                row.Cells[1].Value = rule.Expression;

                dataGridView1.Rows.Add(row);
            }
        }

        private bool isRegexValid(string regex)
        {
            bool isValid;

            if (string.IsNullOrEmpty(regex))
                isValid = false;
            else
            {
                try
                {
                    Regex.IsMatch("", regex);
                    isValid = true;
                }
                catch (ArgumentException ae)
                {
                    isValid = false;
                }
            }
            return isValid;
        }
    }
}
