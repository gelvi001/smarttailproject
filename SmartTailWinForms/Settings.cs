﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SmallTail
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();

            this.dataGridView1.CellClick += new DataGridViewCellEventHandler(dataGridView1_CellClick);
            this.dataGridView1.CellValidated += new DataGridViewCellEventHandler(dataGridView1_CellValidated);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int col = e.ColumnIndex;
            int row = e.RowIndex;

            if (col == 4 && dataGridView1.Rows.GetLastRow(DataGridViewElementStates.Visible) != row && row != -1) // delete
            {
                dataGridView1.Rows.Remove(dataGridView1.Rows[row]);
            }
            else if (col == 3 && dataGridView1.Rows.GetLastRow(DataGridViewElementStates.Visible) != row && row != -1) // backColor
            {
                // if first time opening, set selected to yellow
                DataGridViewCell c = (DataGridViewCell)dataGridView1.Rows[row].Cells[1];
                if (c.Tag != null)
                    colorDialog1.Color = ((ColorPair)c.Tag).BackColor;

                DialogResult result = colorDialog1.ShowDialog();

                if (result == DialogResult.OK)
                {
                    if (c.Tag != null)
                        ((ColorPair)c.Tag).BackColor = colorDialog1.Color;
                    c.Style.BackColor = colorDialog1.Color;
                }
            }
            else if (col == 2 && dataGridView1.Rows.GetLastRow(DataGridViewElementStates.Visible) != row && row != -1) // ForeColor
            {
                DataGridViewCell c = (DataGridViewCell)dataGridView1.Rows[row].Cells[1];
                if (c.Tag != null)
                    colorDialog1.Color = ((ColorPair)c.Tag).ForeColor;

                DialogResult result = colorDialog1.ShowDialog();

                if (result == DialogResult.OK)
                {
                    if (c.Tag != null)
                        ((ColorPair)c.Tag).ForeColor = colorDialog1.Color;
                    c.Style.ForeColor = colorDialog1.Color;
                }
            }
        }

        private void dataGridView1_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCell currentCell = dataGridView1.Rows[e.RowIndex].Cells[1];
            if (e.ColumnIndex == 1 && currentCell.Value != null)
            {
                if (currentCell.Tag == null)
                {
                    currentCell.Tag = new ColorPair(Color.Black, Color.Yellow);

                    currentCell.Style.ForeColor = Color.Black;
                    currentCell.Style.BackColor = Color.Yellow;
                }
                dataGridView1.Rows[e.RowIndex].Cells[0].Value = true;
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            // save to file and update highlighting

            List<HighlightingRule> rules = new List<HighlightingRule>();

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (row.Cells[1].Value != null && row.Cells[1].Tag != null)
                    rules.Add(new HighlightingRule(Convert.ToBoolean(row.Cells[0].Value), (string)row.Cells[1].Value, (ColorPair)row.Cells[1].Tag));
            }

            Form1.highlightingRules = rules;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            // set theme
            this.Opacity = Form1.fOpacity;
            this.BackColor = Form1.fTrim;
            this.dataGridView1.BackgroundColor = Form1.fBackColor;

            this.buttonCancel.BackColor = Form1.fBackColor;
            this.buttonCancel.ForeColor = (Form1.fBackColor == Color.White || Form1.fBackColor == Color.LightCyan) ? Color.Black : Color.White;

            this.buttonSave.BackColor = Form1.fBackColor;
            this.buttonSave.ForeColor = (Form1.fBackColor == Color.White || Form1.fBackColor == Color.LightCyan) ? Color.Black : Color.White;

            // read in rules
            foreach (HighlightingRule rule in Form1.highlightingRules)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGridView1);
                row.Cells[0].Value = rule.Enabled;
                row.Cells[1].Value = rule.Expression;
                row.Cells[1].Tag = rule.Colors;
                row.Cells[1].Style.ForeColor = rule.Colors.ForeColor;
                row.Cells[1].Style.BackColor = rule.Colors.BackColor;

                dataGridView1.Rows.Add(row);
            }
        }
    }

    public class ColorPair
    {
        private Color _backColor;
        private Color _foreColor;

        public Color BackColor
        {
            get { return _backColor; }
            set { _backColor = value; }
        }

        public Color ForeColor
        {
            get { return _foreColor; }
            set { _foreColor = value; }
        }

        public ColorPair(Color foreColor, Color backColor)
        {
            _foreColor = foreColor;
            _backColor = backColor;
        }
    }

    public class HighlightingRule
    {
        private bool _enabled;
        private string _expression;
        private ColorPair _colors;

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public string Expression
        {
            get { return _expression; }
            set { _expression = value; }
        }

        public ColorPair Colors
        {
            get { return _colors; }
            set { _colors = value; }
        }

        public HighlightingRule(bool enabled, string expression, ColorPair colors)
        {
            _enabled = enabled;
            _expression = expression;
            _colors = colors;
        }
    }
}
