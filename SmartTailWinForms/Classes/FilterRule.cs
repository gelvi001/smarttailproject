﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTail
{
    public class FilterRule
    {
        public bool Enabled { get; set; }

        public string Expression { get; set; }

        public FilterRule(bool enabled, string expression)
        {
            Enabled = enabled;
            Expression = expression;
        }
    }
}
