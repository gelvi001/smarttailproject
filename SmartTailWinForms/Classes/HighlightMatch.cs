﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTail
{
    class HighlightMatch
    {
        private int _start;
        private int _length;
        private ColorPair _colors;

        public int Start
        {
            get { return _start; }
            set { _start = value; }
        }

        public int Length
        {
            get { return _length; }
            set { _length = value; }
        }

        public ColorPair Colors
        {
            get { return _colors; }
            set { _colors = value; }
        }

        public HighlightMatch(int start, int length, ColorPair colors)
        {
            _start = start;
            _length = length;
            _colors = colors;
        }
    }
}
