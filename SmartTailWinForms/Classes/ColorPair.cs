﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTail
{
    public class ColorPair
    {
        private Color _backColor;
        private Color _foreColor;

        public Color BackColor
        {
            get { return _backColor; }
            set { _backColor = value; }
        }

        public Color ForeColor
        {
            get { return _foreColor; }
            set { _foreColor = value; }
        }

        public ColorPair(Color foreColor, Color backColor)
        {
            _foreColor = foreColor;
            _backColor = backColor;
        }
    }
}
