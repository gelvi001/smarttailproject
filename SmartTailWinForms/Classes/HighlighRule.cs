﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTail
{
    public class HighlightRule
    {
        private bool _enabled;
        private string _expression;
        private ColorPair _colors;

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public string Expression
        {
            get { return _expression; }
            set { _expression = value; }
        }

        public ColorPair Colors
        {
            get { return _colors; }
            set { _colors = value; }
        }

        public HighlightRule(bool enabled, string expression, ColorPair colors)
        {
            _enabled = enabled;
            _expression = expression;
            _colors = colors;
        }
    }
}
