﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartTail
{
    public class FileScanner
    {

        private bool _IsScanning = false;

        private string _Content = string.Empty;

        public string FilePath { get; set; }

        public string FolderPath { get; set; }

        public bool AutoLoad { get; set; }

        public event EventHandler<string> ContentUpdated;

        public event EventHandler<string> NewFileFound;

        public bool IsScanning { get { return _IsScanning; } }

        public async void StartScan()
        {
            if (_IsScanning) return;
            _IsScanning = true;
            while (_IsScanning)
            {
                if (AutoLoad)
                {
                    // check if directory exists
                    if (!Directory.Exists(FolderPath))
                    {
                        MessageBox.Show("Directory no longer exists:\n" + FolderPath, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        AutoLoad = false;
                        OnNewFileFound(string.Empty);
                        _IsScanning = false;
                        return;
                    }
                    // get most recently modified file
                    DirectoryInfo di = new DirectoryInfo(FolderPath);
                    FileInfo[] fi = di.GetFiles("*.txt");
                    var path = (fi.Length == 0) ? null : fi.OrderByDescending(f => f.LastWriteTime).First().FullName;

                    if (path != FilePath)
                    {
                        FilePath = path;
                        _Content = string.Empty;
                        OnNewFileFound(FilePath);
                    }
                }

                if (!string.IsNullOrEmpty(FilePath))
                {
                    using (FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        using (StreamReader sr = new StreamReader(fs))
                        {
                            var newContent = await sr.ReadToEndAsync();
                            if (newContent.Length > _Content.Length)
                            {
                                var index = _Content.Length;
                                OnContentUpdated(newContent.Substring(index));
                                _Content = newContent;
                            }
                        }
                    }
                }
            }
        }

        public void StopScanning()
        {
            _IsScanning = false;
        }

        protected virtual void OnContentUpdated(string e)
        {
            ContentUpdated?.Invoke(this, e);
        }

        protected virtual void OnNewFileFound(string e)
        {
            NewFileFound?.Invoke(this, e);
        }
    }
}
